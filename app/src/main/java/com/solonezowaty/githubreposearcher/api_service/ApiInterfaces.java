package com.solonezowaty.githubreposearcher.api_service;

import com.solonezowaty.githubreposearcher.main_activity.model.RepoList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Jakub Solecki on 2019-02-12.
 */
public interface ApiInterfaces {

    @GET("/search/repositories")
    Call<RepoList> getSearchedReposList(@Query("q") String query);
}
