package com.solonezowaty.githubreposearcher.details_activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.solonezowaty.githubreposearcher.R;
import com.solonezowaty.githubreposearcher.action_bar.ActionBarActivity;
import com.solonezowaty.githubreposearcher.main_activity.model.RepoItem;
import com.squareup.picasso.Picasso;


public class DetailsActivity extends ActionBarActivity {

    private TextView repoOwnerName;
    private TextView repoName;
    private ImageView gitAvatar;
    private TextView repoDescription;
    private TextView repoLanguage;

    @Override
    protected boolean getBackButtonEnabled() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getLayoutComponents();
        setDataToView();
    }

    private void getLayoutComponents(){
        repoOwnerName = findViewById(R.id.repo_owner_text_input);
        repoName = findViewById(R.id.repo_name_text_input);
        gitAvatar = findViewById(R.id.avatar);
        repoDescription = findViewById(R.id.repo_description_text_input);
        repoLanguage = findViewById(R.id.language_text_input);
    }

    private void setDataToView(){
        RepoItem repoItem = (RepoItem) getIntent().getParcelableExtra("repoItem");
        if (repoItem != null){
            repoOwnerName.setText(repoItem.getOwner().getLogin());
            repoName.setText(repoItem.getName());
            Picasso.with(this).load(repoItem.getOwner().getAvatarUrl()).into(gitAvatar);
            repoDescription.setText(repoItem.getRepoDescription());
            repoLanguage.setText(repoItem.getLanguage());
        }
    }
}
