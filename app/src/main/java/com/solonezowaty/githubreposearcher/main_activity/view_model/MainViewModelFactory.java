package com.solonezowaty.githubreposearcher.main_activity.view_model;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

/**
 * Created by Jakub Solecki on 2019-02-12.
 */
public class MainViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    public MainViewModelFactory() {
    }

    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MainViewModel();
    }
}
