package com.solonezowaty.githubreposearcher.main_activity.interfaces;

/**
 * Created by Jakub Solecki on 2019-02-12.
 */
public interface ApiCallbackStatus {

    void onApiCallStart();
    void onCallSuccess();
    void onCallFailure();
    void onNoInternetConnection();
}
