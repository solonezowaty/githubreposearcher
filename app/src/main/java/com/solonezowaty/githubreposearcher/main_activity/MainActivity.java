package com.solonezowaty.githubreposearcher.main_activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;

import com.solonezowaty.githubreposearcher.R;
import com.solonezowaty.githubreposearcher.main_activity.interfaces.ApiCallbackStatus;
import com.solonezowaty.githubreposearcher.main_activity.model.RepoItem;
import com.solonezowaty.githubreposearcher.main_activity.view_model.MainViewModel;
import com.solonezowaty.githubreposearcher.main_activity.view_model.MainViewModelFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class MainActivity extends AppCompatActivity implements ApiCallbackStatus {

    private View nothingMatchLayout;
    private View recyclerViewLayout;
    private View noConnectionLayout;
    private View loadingLayout;

    private MainViewModel mainViewModel;

    private RecyclerView recyclerView;
    private MainRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainViewModel = ViewModelProviders.of(this, new MainViewModelFactory()).get(MainViewModel.class);
        mainViewModel.addApiCallbackStatusInterface(this);
        getLayoutComponents();
        configRecyclerView();
        initializeObserver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainViewModel.addApiCallbackStatusInterface(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mainViewModel.removeInterface();
    }

    private void getLayoutComponents(){
        recyclerView = findViewById(R.id.repo_recycler_view);
        nothingMatchLayout = findViewById(R.id.nothing_found_info_layout);
        recyclerViewLayout = findViewById(R.id.repo_list_layout);
        noConnectionLayout =findViewById(R.id.no_internet_info_layout);
        loadingLayout = findViewById(R.id.loading_layout);
    }

    private void configRecyclerView(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MainRecyclerAdapter(mainViewModel.getRepoList());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        fromView(searchView)
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter(text -> {
                    if (text.isEmpty()){
                        return false;
                    }else {
                        return true;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> mainViewModel.getReposFromApi(MainActivity.this, result));

        return super.onCreateOptionsMenu(menu);
    }

    public Observable<String> fromView(SearchView searchView) {

        final PublishSubject<String> subject = PublishSubject.create();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String submittedText) {
                subject.onComplete();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String text) {
                subject.onNext(text);
                return true;
            }
        });
        return subject;
    }

    private void initializeObserver(){
        final Observer<List<RepoItem>> repos = repoItems -> adapter.notifyDataSetChanged();
        mainViewModel.getRepoList().observe(this, repos);
    }

    @Override
    public void onApiCallStart() {
        loadingLayout.setVisibility(View.VISIBLE);
        noConnectionLayout.setVisibility(View.GONE);
        recyclerViewLayout.setVisibility(View.GONE);
        nothingMatchLayout.setVisibility(View.GONE);
    }

    @Override
    public void onCallSuccess() {
        loadingLayout.setVisibility(View.GONE);
        noConnectionLayout.setVisibility(View.GONE);
        recyclerViewLayout.setVisibility(View.VISIBLE);
        nothingMatchLayout.setVisibility(View.GONE);
    }

    @Override
    public void onCallFailure() {
        loadingLayout.setVisibility(View.GONE);
        noConnectionLayout.setVisibility(View.GONE);
        recyclerViewLayout.setVisibility(View.GONE);
        nothingMatchLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNoInternetConnection() {
        loadingLayout.setVisibility(View.GONE);
        noConnectionLayout.setVisibility(View.VISIBLE);
        recyclerViewLayout.setVisibility(View.GONE);
        nothingMatchLayout.setVisibility(View.GONE);
    }
}
