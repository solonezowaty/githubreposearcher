package com.solonezowaty.githubreposearcher.main_activity.view_model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.solonezowaty.githubreposearcher.api_service.ApiInterfaces;
import com.solonezowaty.githubreposearcher.api_service.ApiService;
import com.solonezowaty.githubreposearcher.main_activity.interfaces.ApiCallbackStatus;
import com.solonezowaty.githubreposearcher.main_activity.model.RepoItem;
import com.solonezowaty.githubreposearcher.main_activity.model.RepoList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jakub Solecki on 2019-02-12.
 */
public class MainViewModel extends ViewModel {

    private ApiCallbackStatus apiCallbackStatus;
    private MutableLiveData<List<RepoItem>> repoListFromApi;

    public MutableLiveData<List<RepoItem>> getRepoList(){
        if (repoListFromApi == null){
            repoListFromApi = new MutableLiveData<>();
            repoListFromApi.setValue(new ArrayList<RepoItem>());
        }
        return repoListFromApi;
    }

    public void getReposFromApi(Context context, String query){
        apiCallbackStatus.onApiCallStart();
        if(checkConnectionStatus(context)){
            ApiInterfaces apiInterfaces = ApiService.getRetrofit().create(ApiInterfaces.class);
            Call<RepoList> call = apiInterfaces.getSearchedReposList(query);
            call.enqueue(new Callback<RepoList>() {
                @Override
                public void onResponse(Call<RepoList> call, Response<RepoList> response) {
                    if (response.isSuccessful() && response.code() == 200){
                        if (response.body().getItems().size() == 0){
                        apiCallbackStatus.onCallFailure();
                        }else {
                            repoListFromApi.setValue(response.body().getItems());
                            apiCallbackStatus.onCallSuccess();
                        }
                    }else {
                        apiCallbackStatus.onCallFailure();
                    }
                }

                @Override
                public void onFailure(Call<RepoList> call, Throwable t) {
                    apiCallbackStatus.onCallFailure();
                }
            });
        }else {
            apiCallbackStatus.onNoInternetConnection();
        }
    }

    private boolean checkConnectionStatus(Context context){
        ConnectivityManager connectivityManager =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }else {
            return false;
        }
    }

    public void addApiCallbackStatusInterface(ApiCallbackStatus apiCallbackStatus){
        this.apiCallbackStatus = apiCallbackStatus;
    }

    public void removeInterface(){
        apiCallbackStatus = null;
    }
}
