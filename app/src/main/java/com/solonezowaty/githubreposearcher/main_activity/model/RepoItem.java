package com.solonezowaty.githubreposearcher.main_activity.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jakub Solecki on 2019-02-12.
 */
public class RepoItem implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("full_name")
    @Expose
    private String fullName;

    @SerializedName("owner")
    @Expose
    private RepoOwner owner;

    @SerializedName("language")
    @Expose
    private String language;

    @SerializedName("description")
    @Expose
    private String repoDescription;

    protected RepoItem(Parcel in) {
        this.name = in.readString();
        this.fullName = in.readString();
        this.owner = in.readParcelable(RepoOwner.class.getClassLoader());
        this.language = in.readString();
        this.repoDescription = in.readString();

    }

    public static final Creator<RepoItem> CREATOR = new Creator<RepoItem>() {
        @Override
        public RepoItem createFromParcel(Parcel in) {
            return new RepoItem(in);
        }

        @Override
        public RepoItem[] newArray(int size) {
            return new RepoItem[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public RepoOwner getOwner() {
        return owner;
    }

    public void setOwner(RepoOwner owner) {
        this.owner = owner;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRepoDescription() {
        return repoDescription;
    }

    public void setRepoDescription(String repoDescription) {
        this.repoDescription = repoDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(name);
        parcel.writeString(fullName);
        parcel.writeParcelable(owner, flags);
        parcel.writeString(language);
        parcel.writeString(repoDescription);
    }
}
