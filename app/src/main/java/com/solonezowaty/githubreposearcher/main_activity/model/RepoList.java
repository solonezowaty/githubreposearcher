package com.solonezowaty.githubreposearcher.main_activity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jakub Solecki on 2019-02-12.
 */
public class RepoList {

    @SerializedName("items")
    @Expose
    private List<RepoItem> items = null;

    public List<RepoItem> getItems() {
        return items;
    }

    public void setItems(List<RepoItem> items) {
        this.items = items;
    }
}
