package com.solonezowaty.githubreposearcher.main_activity;

import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.solonezowaty.githubreposearcher.R;
import com.solonezowaty.githubreposearcher.details_activity.DetailsActivity;
import com.solonezowaty.githubreposearcher.main_activity.model.RepoItem;

import java.util.List;

/**
 * Created by Jakub Solecki on 13.02.2019.
 */
public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder> {

    private MutableLiveData<List<RepoItem>> reposList;
    private RepoItem repoItem;

    public MainRecyclerAdapter(MutableLiveData<List<RepoItem>> reposList) {
        this.reposList = reposList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView repositoryName;
        TextView ownerName;
        View detailsButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            repositoryName = itemView.findViewById(R.id.repository_name_input);
            ownerName = itemView.findViewById(R.id.owner_name_input);
            detailsButton = itemView.findViewById(R.id.details_button);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        repoItem = reposList.getValue().get(position);
        holder.repositoryName.setText(repoItem.getName());
        holder.ownerName.setText(repoItem.getOwner().getLogin());
        holder.detailsButton.setOnClickListener(view -> {
            repoItem = reposList.getValue().get(holder.getAdapterPosition());
            Intent intent = new Intent(view.getContext(), DetailsActivity.class);
            intent.putExtra("repoItem", repoItem);
            view.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return reposList.getValue().size();
    }
}
